const path = require('path')
const url = require('url')
const express = require('express')
const app = express()
app.use(express.static('public'))
const port = 3030

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.get('/', (req, res) => res.sendFile('index.html', {root: __dirname}))

app.post('/register', (req, res) => {console.log(req.body)
    res.sendFile(path.join(__dirname, 'public/thank-you.html'))})


//
// app.get('/register', (req, res) => {
//     let url_parts = url.parse(req.url, true)
//     console.log(url_parts.query)
//     res.sendFile(path.join(__dirname, 'public/thank-you.html'));
// })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))